# Despliegue agentes monitorización

## Organización de hostgroups
Tenemos dos ramas organiztivas:
 - technical/
 - functional/

Las ``entities`` deben meterse a mano en los hostgroups de la rama ``functional``. Ejemplo:
```
functional/ENV/ORG/FUNC
functional/ENV/FUNC/ORG
functional/ORG/ENV/FUNC
functional/ORG/FUNC/ENV
functional/FUNC/ENV/ORG
functional/FUNC/ORG/ENV
```

Disponemos de la variable ``functional_host_groups`` donde podemos definir el array de hostgroups funcionales a añadir.
Está pendiente de desarrollo el que esos hostgroups se generen automáticamente a partir de un array de "FUNCs" (necesario?).

Los roles usan el filter_plugin ``tech_hostgroups_generator`` para generar el listado de hostgroups donde debe ir la 'entity' para la rama ``technical``.

Para generar este listado de hostgroups hace falta pasar las variables:
 - env
 - org (por defecto se pone a "default")
 - classx (class es palabra reservada)
 - subclass

Por ahora no se admiten varias orgs (implementar si fuese necesario).

Ejemplo:
```
  Vars:
    env: prod
    org: gce
    classx: db
    subclass: postgres

  Hostgroups:
    technical/prod
    technical/gce
    technical/prod/gce
    technical/gce/prod
    technical/prod/db/postgres
    technical/db/postgres/prod
    technical/gce/db/postgres
    technical/db/postgres/gce
    technical/prod/gce/db/postgres
    technical/prod/db/postgres/gce
    technical/gce/prod/db/postgres
    technical/gce/db/postgres/prod
    technical/db/postgres/prod/gce
    technical/db/postgres/gce/prod
```


Ejemplo de uso al registrar el nodo en zabbix:
defaults/main.yml
```
classx: 'mid'
subclass: 'jboss'
zabbix_app_host_groups: "{{ '' | tech_hostgroups_generator(env, org, classx, subclass) }}"
```

tasks/config_and_register.yml:
```
    - name: "{{server.conf.server_name}} | Register a new host"
      local_action:
        module: zabbix_host
        force: false
        host_groups: "{{ zabbix_app_host_groups }} + {{ functional_host_groups | default([]) }}"
        ...
```
