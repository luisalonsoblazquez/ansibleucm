#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8

from itertools import permutations

def tech_hostgroups_generator(values, env, org, classx, subclass):
    """
    Generador de hostgroups de la rama technical
    A partir de un entorno (prod, cuali, etc), una org (default, dsi, gce, etc),
    una clase (db, app, etc) y una subclase (postgresql, jboss, etc), genera
    un array de hostgroups a los que el host 'entity' debe pertenecer.

    Ejemplo:
        env: prod
        org: gce
        classx: db
        subclass: postgres

    Hostgroups:
        technical/prod
        technical/gce
        technical/prod/gce
        technical/gce/prod
        technical/db/postgress
        technical/prod/db/postgress
        technical/db/postgress/prod
        technical/gce/db/postgress
        technical/db/postgress/gce
        technical/prod/gce/db/postgress
        technical/prod/db/postgress/gce
        technical/gce/prod/db/postgress
        technical/gce/db/postgress/prod
        technical/db/postgress/prod/gce
        technical/db/postgress/gce/prod
    """
    claves_generales = [env, org]
    claves_tecnical = ["%s/%s" % (classx, subclass)]

    hostgroups = ["technical/%s" % x for x in claves_generales]
    hostgroups.extend(["technical/{}".format('/'.join(x)) for x in permutations(claves_generales)])
    hostgroups.extend(["technical/{}".format('/'.join(x)) for x in permutations(claves_tecnical)])
    hostgroups.extend(["technical/{}".format('/'.join(x)) for x in permutations([claves_generales[0]]+claves_tecnical)])
    hostgroups.extend(["technical/{}".format('/'.join(x)) for x in permutations([claves_generales[1]]+claves_tecnical)])
    hostgroups.extend(["technical/{}".format('/'.join(x)) for x in permutations(claves_generales+claves_tecnical)])

    return hostgroups

def apps_from_facts(software):
    """
    Retorna un array con los nombres de aplicaciones vistas en el array de facts de software
    Los nombres seran los del defaults/main.yml software_list del os-facts
    """
    apps = set()
    for app in software:
        apps.add(app["type"])
    return list(apps)


class FilterModule(object):
    def filters(self):
        return {
                'tech_hostgroups_generator': tech_hostgroups_generator,
                'apps_from_facts': apps_from_facts,
        }

