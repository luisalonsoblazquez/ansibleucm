# 1.1.0
* Read logs from end of file(if this file is already registered by filebeat, it will continue from that point and ignore tail). This avoids overload of logs filling up redis and creating trash indices in elasticsearch, as there are unrotated files with very old data.

# 1.0.0
* Standarize all role versions.
